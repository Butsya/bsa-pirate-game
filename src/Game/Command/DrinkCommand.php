<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 0:36
 */

namespace BinaryStudioAcademy\Game\Command;


use BinaryStudioAcademy\Game\Builder\Parts\Ship;
use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Mapper\ShipMapperInterface;

class DrinkCommand implements Command
{
    private $writer;
    private $shipMapper;

    public function __construct(Writer $writer, ShipMapperInterface $shipMapper)
    {
        $this->writer = $writer;
        $this->shipMapper = $shipMapper;
    }

    public function execute()
    {
        $playerShip = $this->shipMapper->getShip('player');
        $currentHealth = $playerShip->getStat('health');
        $hold = $playerShip->getStat('hold');
        $newHealth = $currentHealth + 30 >= 100 ? 100 : $currentHealth + 30;

        $this->writer->writeln("You\'ve drunk a rum and your health filled to {$newHealth}");

        $hold = preg_replace('/' . Ship::RUM . '/', Ship::EMPTY, $hold, 1);

        $newHold = Ship::formatHold($hold);
        $playerShip->setStat('hold', $newHold);
        $playerShip->setStat('health', $newHealth);

        $this->shipMapper->setShip('player', $playerShip);
    }
}
