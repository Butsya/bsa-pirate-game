<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 9:09
 */

namespace BinaryStudioAcademy\Game\Command;


use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class ExitCommand implements Command
{
    private $writer;

    public function __construct(Writer $writer)
    {
        $this->writer = $writer;
    }
    public function execute()
    {
        $this->writer->writeln("You have exit the game!");
        exit();
    }
}