<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 9:47
 */

namespace BinaryStudioAcademy\Game\Command;


use BinaryStudioAcademy\Game\Builder\Parts\Ship;
use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Helpers\Map;
use BinaryStudioAcademy\Game\Contracts\Helpers\Math;
use BinaryStudioAcademy\Game\Contracts\Helpers\PlayerPositionInterface;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Mapper\ShipMapperInterface;

class FireCommand implements Command
{
    private $shipMapper;
    private $writer;
    private $random;
    private $math;
    private $playerPosition;

    public function __construct(
        ShipMapperInterface $shipMapper,
        Writer $writer,
        Math $math,
        Random $random,
        PlayerPositionInterface $playerPosition
    ) {
        $this->shipMapper = $shipMapper;
        $this->writer = $writer;
        $this->math = $math;
        $this->random = $random;
        $this->playerPosition = $playerPosition;
    }

    public function execute()
    {
        $playerShip = $this->shipMapper->getShip('player');
        $enemyShip = $this->shipMapper->getShip('enemy');
        $playerStats = $this->getShipStatsValues($playerShip);
        $enemyStats = $this->getShipStatsValues($enemyShip);
        $isPlayerHit = $this->math->luck($this->random, $playerStats['luck']);
        $isEnemyHit = $this->math->luck($this->random, $enemyStats['luck']);
        $shipName = $this->getShipName();

        if ($isEnemyHit) {
            $enemyDamage = $this->math->damage($enemyStats['strength'], $playerStats['armour']);
            $playerHealth = $playerStats['health'] - $enemyDamage;
        }

        if ($isPlayerHit) {
            $playerDamage = $this->math->damage($playerStats['strength'], $enemyStats['armour']);
            $enemyHealth = $enemyStats['health'] - $playerDamage;
        }

        if ($isPlayerHit && $playerHealth >= 0) {
            if ($enemyHealth <= 0 && $this->isFinalBattle()) {
                $this->writer->write('🎉🎉🎉Congratulations🎉🎉🎉' . PHP_EOL
                    . '💰💰💰 All gold and rum of Great Britain belong to you! 🍾🍾🍾' . PHP_EOL);
            } elseif($enemyHealth <= 0) {
                $this->writer->write("{$shipName} on fire. Take it to the boarding." . PHP_EOL);
            } else {
                $this->writer->write("{$shipName} has damaged on: {$playerDamage} points." . PHP_EOL
                    . "health: {$enemyHealth}" . PHP_EOL);
            }

            $enemyShip->setStat('health', $enemyHealth);
            $this->shipMapper->setShip('enemy', $enemyShip);
        }

        if ($isEnemyHit && $enemyHealth > 0) {
            if ($playerHealth <= 0) {
                $this->writer->write("Your ship has been sunk." . PHP_EOL
                    . "You restored in the Pirate Harbor." . PHP_EOL
                    . "You lost all your possessions and 1 of each stats." . PHP_EOL);
                $this->decreasePlayerStatsAndReturnToPirateHarbor($playerStats);
            } else {
                $this->writer->write("{$shipName} damaged your ship on: {$enemyDamage} points." . PHP_EOL
                    . "health: {$playerHealth}" . PHP_EOL);
                $playerShip->setStat('health', $playerHealth);
                $this->shipMapper->setShip('player', $playerShip);
            }
        }
        
    }

    private function getShipStatsValues(Ship $ship)
    {
        $shipStats = [];

        foreach (Map::SHIPS['schooner']['stats'] as $stat => $value) {
            $shipStats[$stat] = $ship->getStat($stat);
        }

        return $shipStats;
    }

    private function getShipName()
    {
        $harbor = Map::HARBORS[$this->playerPosition->getPosition()];

        return Map::SHIPS[$harbor['ship']]['name'];
    }

    private function decreasePlayerStatsAndReturnToPirateHarbor(array $playerStats)
    {
        $playerShip = $this->shipMapper->getShip('player');

        foreach ($playerStats as $stat => $value) {
            $playerShip->setStat($stat, $value-1);
        }

        $playerShip->setStat('health', 60);
        $playerShip->setStat('hold', '[ ' . Ship::EMPTY . ' ' . Ship::EMPTY . ' ' . Ship::EMPTY . ' ]');
        $this->playerPosition->setPosition(1);
        $this->shipMapper->setShip('player', $playerShip);
    }

    private function isFinalBattle()
    {
        return $this->playerPosition->getPosition() === 8;
    }
}
