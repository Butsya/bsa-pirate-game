<?php

namespace BinaryStudioAcademy\Game\Command\Errors;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class HoldIsFullCommand implements Command
{
    private $writer;

    public function __construct(Writer $writer)
    {
        $this->writer = $writer;
    }

    public function execute()
    {
        $this->writer->writeln('Your hold is full, please use some items to make it empty');
    }
}
