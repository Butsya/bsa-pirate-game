<?php

namespace BinaryStudioAcademy\Game\Command\Errors;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class ShipIsAlreadySunkedCommand implements Command
{
    private $writer;

    public function __construct(Writer $writer)
    {
        $this->writer = $writer;
    }

    public function execute()
    {
        $this->writer->writeln("Ship is already sunked in this harbor.");
    }
}
