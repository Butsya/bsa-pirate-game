<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 07.07.19
 * Time: 1:39
 */

namespace BinaryStudioAcademy\Game\Command\Errors;


use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class NotEnoughGoldCommand implements Command
{
    private $writer;

    public function __construct(Writer $writer)
    {
        $this->writer = $writer;
    }

    public function execute()
    {
        $this->writer->writeln("You don't have enough gold in the hold to buy an item!");
    }
}