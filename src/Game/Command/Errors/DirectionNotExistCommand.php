<?php

namespace BinaryStudioAcademy\Game\Command\Errors;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class DirectionNotExistCommand implements Command
{
    private $writer;
    private $direction;

    public function __construct(Writer $writer, string $direction)
    {
        $this->writer = $writer;
        $this->direction = $direction;
    }

    public function execute()
    {
        $this->writer->writeln("Direction '{$this->direction}' incorrect, choose from: east, west, north, south");
    }
}
