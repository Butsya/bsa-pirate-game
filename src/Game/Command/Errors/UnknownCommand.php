<?php

namespace BinaryStudioAcademy\Game\Command\Errors;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class UnknownCommand implements Command
{
    private $writer;
    private $command;

    public function __construct(Writer $writer, string $command)
    {
        $this->writer = $writer;
        $this->command = $command;
    }

    public function execute()
    {
        $this->writer->writeln("Command '{$this->command}' not found");
    }
}
