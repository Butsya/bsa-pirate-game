<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 07.07.19
 * Time: 1:39
 */

namespace BinaryStudioAcademy\Game\Command\Errors;


use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class SkillMaxValueCommand implements Command
{
    private $writer;
    private $skill;

    public function __construct(Writer $writer, string $skill)
    {
        $this->writer = $writer;
        $this->skill = $skill;
    }

    public function execute()
    {
        $this->writer->writeln("You can't upgrade {$this->skill}, it's maximum level");
    }
}