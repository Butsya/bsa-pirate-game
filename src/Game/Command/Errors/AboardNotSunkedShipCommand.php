<?php

namespace BinaryStudioAcademy\Game\Command\Errors;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class AboardNotSunkedShipCommand implements Command
{
    private $writer;

    public function __construct(Writer $writer)
    {
        $this->writer = $writer;
    }

    public function execute()
    {
        $this->writer->writeln('You cannot board this ship, since it has not yet sunk');
    }
}
