<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 9:34
 */

namespace BinaryStudioAcademy\Game\Command;


use BinaryStudioAcademy\Game\Builder\Director;
use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Helpers\Map;
use BinaryStudioAcademy\Game\Contracts\Helpers\PlayerPositionInterface;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Mapper\ShipMapperInterface;

class SetSailCommand implements Command
{
    private $writer;
    private $shipMapper;
    private $playerPosition;
    private $direction;

    public function __construct(
        Writer $writer,
        ShipMapperInterface $shipMapper,
        PlayerPositionInterface $playerPosition,
        string $direction
    ) {
        $this->shipMapper = $shipMapper;
        $this->writer = $writer;
        $this->direction = $direction;
        $this->playerPosition = $playerPosition;
    }

    public function execute()
    {
        foreach (Map::MAP[$this->playerPosition->getPosition()] as $path => $newPosition) {
            if ($this->direction === $path) {
                $director = new Director();
                $this->playerPosition->setPosition($newPosition);
                $harbor = Map::HARBORS[$newPosition];
                $ship = Map::SHIPS[$harbor['ship']];
                if (isset($ship['shipBuilder'])) {
                    $this->shipMapper->setShip('enemy', $director->build(new $ship['shipBuilder']()));
                }
                $playerShip = $this->shipMapper->getShip('player');
                $enemyShip = $this->shipMapper->getShip('enemy');

                if ($newPosition === 1) {
                    $this->writer->write('Harbor 1: Pirates Harbor.' . PHP_EOL);
                    if ($playerShip->getStat('health') < 60) {
                        $playerShip->setStat('health', 60);
                        $this->writer->write('Your health is repared to 60.' . PHP_EOL);
                        $this->shipMapper->setShip('player', $playerShip);
                    }
                } else {
                    $this->writer->write("Harbor {$newPosition}: {$harbor['harbor']}." . PHP_EOL
                        . "You see {$ship['name']}: " . PHP_EOL
                        . 'strength: ' . $enemyShip->getStat('strength') . PHP_EOL
                        . 'armour: ' . $enemyShip->getStat('armour') . PHP_EOL
                        . 'luck: ' . $enemyShip->getStat('luck')  . PHP_EOL
                        . 'health: ' . $enemyShip->getStat('health')   . PHP_EOL);
                }
            }
        }

    }
}
