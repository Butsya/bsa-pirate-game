<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 0:54
 */

namespace BinaryStudioAcademy\Game\Command;


use BinaryStudioAcademy\Game\Builder\Parts\Ship;
use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Mapper\ShipMapperInterface;

class BuySkillCommand implements Command
{
    private $writer;
    private $shipMapper;
    private $item;

    public function __construct(Writer $writer, ShipMapperInterface $shipMapper, string $item)
    {
        $this->writer = $writer;
        $this->shipMapper = $shipMapper;
        $this->item = $item;
    }

    public function execute()
    {
        $playerShip = $this->shipMapper->getShip('player');
        $hold = $playerShip->getStat('hold');
        $hold = preg_replace('/' . Ship::GOLD . '/', Ship::EMPTY, $hold, 1);
        var_dump($hold);

        $newHold = Ship::formatHold($hold);
        $playerShip->setStat('hold', $newHold);
        $newSkillValue = $playerShip->getStat($this->item) + 1;
        $playerShip->setStat($this->item, $newSkillValue);
        $this->writer->writeln("You\'ve bought a {$this->item}. Your {$this->item} is {$newSkillValue}.");

        $this->shipMapper->setShip('player', $playerShip);
    }
}
