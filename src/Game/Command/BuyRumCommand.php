<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 0:54
 */

namespace BinaryStudioAcademy\Game\Command;


use BinaryStudioAcademy\Game\Builder\Parts\Ship;
use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Mapper\ShipMapperInterface;

class BuyRumCommand implements Command
{
    private $writer;
    private $shipMapper;

    public function __construct(Writer $writer, ShipMapperInterface $shipMapper)
    {
        $this->writer = $writer;
        $this->shipMapper = $shipMapper;
    }

    public function execute()
    {
        $playerShip = $this->shipMapper->getShip('player');
        $hold = $playerShip->getStat('hold');
        $hold = preg_replace('/' . Ship::GOLD . '/', Ship::RUM, $hold, 1);
        var_dump($hold);

        $newHold = Ship::formatHold($hold);
        $newRumValue = substr_count($newHold, Ship::RUM);

        $this->writer->writeln("You\'ve bought a rum. Your hold contains {$newRumValue} bottle(s) of rum.");
        $playerShip->setStat('hold', $newHold);
        $this->shipMapper->setShip('player', $playerShip);
    }
}
