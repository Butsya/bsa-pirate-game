<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 22:49
 */

namespace BinaryStudioAcademy\Game\Command;

use BinaryStudioAcademy\Game\Builder\Parts\Ship;
use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class StatsCommand implements Command
{
    private $writer;

    private $ship;

    public function __construct(Writer $writer, Ship $ship)
    {
        $this->writer = $writer;
        $this->ship = $ship;
    }


    public function execute()
    {
        $this->writer->writeln('Ship stats:');
        $this->writer->writeln('strength: ' . $this->ship->getStat('strength'));
        $this->writer->writeln('armour: ' . $this->ship->getStat('armour'));
        $this->writer->writeln('luck: ' . $this->ship->getStat('luck'));
        $this->writer->writeln('health: ' . $this->ship->getStat('health'));
        $this->writer->writeln('hold: ' . $this->ship->getStat('hold'));
    }
}