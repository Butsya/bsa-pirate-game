<?php

namespace BinaryStudioAcademy\Game\Command;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class HelpCommand implements Command
{
    private $writer;

    public function __construct(Writer $writer)
    {
        $this->writer = $writer;
    }

    public function execute()
    {
        $this->writer->writeln('List of commands:');
        $this->writer->writeln('help - shows this list of commands');
        $this->writer->writeln('stats - shows stats of ship');
        $this->writer->writeln('set-sail <east|west|north|south> - moves in given direction');
        $this->writer->writeln('fire - attacks enemy\'s ship');
        $this->writer->writeln('aboard - collect loot from the ship');
        $this->writer->writeln('buy <strength|armour|luck|rum> - buys skill or rum: 1 chest of gold - 1 item');
        $this->writer->writeln('drink - your captain drinks 1 bottle of rum and fill 30 points of health');
        $this->writer->writeln('whereami - shows current harbor');
        $this->writer->writeln('exit - finishes the game');
    }
}