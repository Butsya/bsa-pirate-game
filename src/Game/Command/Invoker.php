<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 07.07.19
 * Time: 2:37
 */

namespace BinaryStudioAcademy\Game\Command;

use BinaryStudioAcademy\Game\Contracts\Command\Command;

class Invoker implements \BinaryStudioAcademy\Game\Contracts\Command\Invoker
{
    /**
     * @var Command
     */
    private $command;

    public function setCommand(Command $cmd)
    {
        $this->command = $cmd;
    }

    public function run()
    {
        $this->command->execute();
    }
}