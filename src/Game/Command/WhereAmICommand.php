<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 0:24
 */

namespace BinaryStudioAcademy\Game\Command;



use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Helpers\Map;
use BinaryStudioAcademy\Game\Contracts\Helpers\PlayerPositionInterface;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class WhereAmICommand implements Command
{
    private $writer;
    private $playerPosition;

    public function __construct(Writer $writer, PlayerPositionInterface $playerPosition)
    {
        $this->writer = $writer;
        $this->playerPosition = $playerPosition;
    }

    public function execute()
    {
        $harborNumber = $this->playerPosition->getPosition();
        $harbor = Map::HARBORS[$harborNumber];
        $this->writer->writeln("Harbor {$harborNumber}: {$harbor['harbor']}");
    }
}