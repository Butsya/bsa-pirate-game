<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 13:16
 */

namespace BinaryStudioAcademy\Game\Command;


use BinaryStudioAcademy\Game\Builder\Parts\Ship;
use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Mapper\ShipMapperInterface;

class AboardCommand implements Command
{
    private $shipMapper;
    private $writer;

    public function __construct(ShipMapperInterface $shipMapper, Writer $writer)
    {
        $this->shipMapper = $shipMapper;
        $this->writer = $writer;
    }
    public function execute()
    {

        $playerHold = $this->shipMapper->getShip('player')->getStat('hold');
        $enemyHold = $this->shipMapper->getShip('enemy')->getStat('hold');

        if ($this->isGoldExistInTheHold($enemyHold)) {
            $this->takeItem($playerHold, $enemyHold, Ship::GOLD);
            $this->writer->write('You got 💰.' . PHP_EOL);
        } elseif ($this->isRumExistInTheHold($enemyHold)) {
            $this->takeItem($playerHold, $enemyHold, Ship::RUM);
            $this->writer->write('You got 🍾.' . PHP_EOL);
        }
    }

    private function isRumExistInTheHold(string $hold)
    {
        return strpos($hold, Ship::RUM);
    }

    private function isGoldExistInTheHold(string $hold)
    {
        return strpos($hold, Ship::GOLD);
    }

    private function takeItem(string $playerHold, string $enemyHold, string $itemName)
    {
        $playerShip = $this->shipMapper->getShip('player');
        $enemyShip = $this->shipMapper->getShip('enemy');

        $newPlayerHold = preg_replace('/' . Ship::EMPTY . '/', $itemName, $playerHold, 1);
        $newEnemyHold = preg_replace('/' . Ship::EMPTY . '/', $itemName, $enemyHold, 1);

        $playerShip->setStat('hold', $newPlayerHold);
        $enemyShip->setStat('hold', $newEnemyHold);

        $this->shipMapper->setShip('player', $playerShip);
        $this->shipMapper->setShip('enemy', $enemyShip);
    }
}
