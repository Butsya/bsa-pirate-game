<?php
declare(strict_types=1);

namespace BinaryStudioAcademy\Game\Mapper;

use BinaryStudioAcademy\Game\Builder\Parts\Ship;
use BinaryStudioAcademy\Game\Contracts\Mapper\ShipMapperInterface;

class ShipMapper implements ShipMapperInterface
{
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function setShip(string $type, Ship $ship)
    {
        $this->data[$type] = $ship;
    }


    public function getShip(string $type): Ship
    {
        return $this->data[$type];
    }
}
