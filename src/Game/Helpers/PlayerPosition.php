<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 07.07.19
 * Time: 0:39
 */

namespace BinaryStudioAcademy\Game\Helpers;


use BinaryStudioAcademy\Game\Contracts\Helpers\PlayerPositionInterface;

class PlayerPosition implements PlayerPositionInterface
{
    private $position = 1;

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition(int $position)
    {
        $this->position = $position;
    }
}