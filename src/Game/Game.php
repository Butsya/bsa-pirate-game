<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Builder\Director;
use BinaryStudioAcademy\Game\Builder\PirateShipBuilder;
use BinaryStudioAcademy\Game\Builder\RoyalPatrolSchoonerShipBuilder;
use BinaryStudioAcademy\Game\Command\Invoker;
use BinaryStudioAcademy\Game\CommandFactory\CommandFactory;
use BinaryStudioAcademy\Game\Contracts\Helpers\PlayerPositionInterface;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Mapper\ShipMapperInterface;
use BinaryStudioAcademy\Game\Helpers\Math;
use BinaryStudioAcademy\Game\Helpers\PlayerPosition;
use BinaryStudioAcademy\Game\Mapper\ShipMapper;

class Game
{
    private $random;

    public function __construct(Random $random)
    {
        $this->random = $random;
    }

    public function start(Reader $reader, Writer $writer)
    {
        $director = new Director();
        $playerPosition = new PlayerPosition();
        $invoker = new Invoker();
        $math = new Math();
        $shipMapper = new ShipMapper([]);
        $shipMapper->setShip('player', $director->build(new PirateShipBuilder()));
        $shipMapper->setShip('enemy', $director->build(new RoyalPatrolSchoonerShipBuilder()));
        $writer->writeln('Your task is to develop a game "Battle Ship".');
        $writer->writeln('This method starts infinite loop with game logic.');
        $writer->writeln('Use proposed implementation in order to tests work correct.');
        $writer->writeln('Random float number: ' . $this->random->get());
        $writer->writeln('Feel free to remove this lines and write yours instead.');
        $writer->writeln('Adventure has begun. Wish you good luck!');
        $writer->writeln('Use `help` command to get information about available commands or start playing');
        $input = trim($reader->read());
        if ($input === "") {
            $writer->writeln("Use `help` command to get information about available commands or start playing");
            $input = trim($reader->read());
        }


        while ($input) {
            $commandFactory = new CommandFactory(
                $writer, $playerPosition, $math, $this->random, $shipMapper, $input
            );
            $invoker->setCommand($commandFactory->createCommand());
            $invoker->run();
            $writer->writeln('Please enter next command... ');
            $input = trim($reader->read());
        }
    }

    public function run(
        Reader $reader,
        Writer $writer,
        PlayerPositionInterface $playerPosition,
        \BinaryStudioAcademy\Game\Contracts\Helpers\Math $math,
        ShipMapperInterface $shipMapper,
        Invoker $invoker
    )
    {
        //$writer->writeln('This method runs program step by step.');
        $input = trim($reader->read());
        $commandFactory = new CommandFactory(
            $writer, $playerPosition, $math, $this->random, $shipMapper, $input
        );
        $invoker->setCommand($commandFactory->createCommand());
        $invoker->run();
    }
}
