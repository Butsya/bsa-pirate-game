<?php

namespace BinaryStudioAcademy\Game\Builder;

use BinaryStudioAcademy\Game\Builder\Parts\PirateShip;
use BinaryStudioAcademy\Game\Builder\Parts\Ship;
use BinaryStudioAcademy\Game\Contracts\Builder\BuilderInterface;

class PirateShipBuilder implements BuilderInterface
{
    /**
     * @var PirateShip
     */
    private $ship;

    public function createShip()
    {
        $this->ship = new PirateShip();
    }

    public function addStrength()
    {
        $this->ship->setStat('strength', 4);
    }

    public function addArmour()
    {
        $this->ship->setStat('armour', 4);
    }

    public function addLuck()
    {
        $this->ship->setStat('luck', 4);
    }

    public function addHealth()
    {
        $this->ship->setStat('health', 60);
    }

    public function addHold()
    {
        $this->ship->setStat('hold', '[ ' . Ship::EMPTY . ' ' . Ship::EMPTY . ' ' . Ship::EMPTY . ' ]');
    }

    public function getShip(): Ship
    {
        return $this->ship;
    }
}
