<?php
/**
 * Created by PhpStorm.
 * User: bucya
 * Date: 7/5/19
 * Time: 5:27 PM
 */

namespace BinaryStudioAcademy\Game\Builder;

use BinaryStudioAcademy\Game\Builder\Parts\Ship;
use BinaryStudioAcademy\Game\Builder\Parts\RoyalBattleShip;
use BinaryStudioAcademy\Game\Contracts\Builder\BuilderInterface;

class RoyalBattleShipBuilder implements BuilderInterface
{
    /**
     * @var RoyalBattleShip
     */
    private $ship;

    public function createShip()
    {
        $this->ship = new RoyalBattleShip();
    }

    public function addStrength()
    {
        $this->ship->setStat('strength', 8);
    }

    public function addArmour()
    {
        $this->ship->setStat('armour', 8);
    }

    public function addLuck()
    {
        $this->ship->setStat('luck', 7);
    }

    public function addHealth()
    {
        $this->ship->setStat('health', 80);
    }

    public function addHold()
    {
        $this->ship->setStat('hold', '[ ' . Ship::RUM . ' ' . Ship::EMPTY . ' ' . Ship::EMPTY . ' ]');
    }

    public function getShip(): Ship
    {
        return $this->ship;
    }
}
