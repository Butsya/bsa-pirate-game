<?php
/**
 * Created by PhpStorm.
 * User: bucya
 * Date: 7/5/19
 * Time: 5:23 PM
 */

namespace BinaryStudioAcademy\Game\Builder;


use BinaryStudioAcademy\Game\Builder\Parts\RoyalPatrolSchoonerShip;
use BinaryStudioAcademy\Game\Builder\Parts\Ship;
use BinaryStudioAcademy\Game\Contracts\Builder\BuilderInterface;

class RoyalPatrolSchoonerShipBuilder implements BuilderInterface
{
    /**
     * @var RoyalPatrolSchoonerShip
     */
    private $ship;

    public function createShip()
    {
        $this->ship = new RoyalPatrolSchoonerShip();
    }

    public function addStrength()
    {
        $this->ship->setStat('strength', 4);
    }

    public function addArmour()
    {
        $this->ship->setStat('armour', 4);
    }

    public function addLuck()
    {
        $this->ship->setStat('luck', 4);
    }

    public function addHealth()
    {
        $this->ship->setStat('health', 50);
    }

    public function addHold()
    {
        $this->ship->setStat('hold', '[ ' . Ship::GOLD . ' ' . Ship::EMPTY . ' ' . Ship::EMPTY . ' ]');
    }

    public function getShip(): Ship
    {
        return $this->ship;
    }
}
