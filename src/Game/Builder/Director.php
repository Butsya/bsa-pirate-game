<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 22:51
 */

namespace BinaryStudioAcademy\Game\Builder;

use BinaryStudioAcademy\Game\Builder\Parts\Ship;
use BinaryStudioAcademy\Game\Contracts\Builder\BuilderInterface;

class Director
{
    public function build(BuilderInterface $builder): Ship
    {
        $builder->createShip();
        $builder->addStrength();
        $builder->addArmour();
        $builder->addLuck();
        $builder->addHealth();
        $builder->addHold();

        return $builder->getShip();
    }
}