<?php

namespace BinaryStudioAcademy\Game\Builder\Parts;


abstract class Ship
{
    const RUM = '🍾';
    const GOLD = '💰';
    const EMPTY = '_';

    private $data = [];

    /**
     * @param string $stat
     * @param int|array $value
     */
    public function setStat(string $stat, $value)
    {
        $this->data[$stat] = $value;
    }

    public function getStat(string $stat)
    {
        return $this->data[$stat];
    }

    public static function formatHold(string $hold): string
    {
        $removedItems = substr_count($hold, Ship::EMPTY);
        $hold = explode(' ', str_replace(Ship::EMPTY, false, $hold));
        array_pop($hold);
        array_shift($hold);
        $hold = array_filter($hold);

        for ($i = 0; $i < $removedItems; $i++) {
            array_push($hold, Ship::EMPTY);
        }

        return '[ ' . trim(implode(' ', $hold)) . ' ]';
    }
}
