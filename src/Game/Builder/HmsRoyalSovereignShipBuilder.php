<?php
/**
 * Created by PhpStorm.
 * User: bucya
 * Date: 7/5/19
 * Time: 5:29 PM
 */

namespace BinaryStudioAcademy\Game\Builder;


use BinaryStudioAcademy\Game\Builder\Parts\HmsRoyalSovereignShip;
use BinaryStudioAcademy\Game\Builder\Parts\Ship;
use BinaryStudioAcademy\Game\Contracts\Builder\BuilderInterface;

class HmsRoyalSovereignShipBuilder implements BuilderInterface
{
    /**
     * @var HmsRoyalSovereignShip
     */
    private $ship;
    
    public function createShip()
    {
        $this->ship = new HmsRoyalSovereignShip();
    }

    public function addStrength()
    {
        $this->ship->setStat('strength', 10);
    }

    public function addArmour()
    {
        $this->ship->setStat('armour', 10);
    }

    public function addLuck()
    {
        $this->ship->setStat('luck', 10);
    }

    public function addHealth()
    {
        $this->ship->setStat('health', 100);
    }

    public function addHold()
    {
        $this->ship->setStat('hold', '[ ' . Ship::GOLD . ' ' . Ship::GOLD . ' ' . Ship::RUM . ' ]');
    }

    public function getShip(): Ship
    {
        return $this->ship;
    }
}
