<?php

namespace BinaryStudioAcademy\Game\CommandFactory\Errors;

use BinaryStudioAcademy\Game\Command\Errors\UnknownCommand;
use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\CommandFactory\CommandFactoryInterface;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class UnknownCommandFactory implements CommandFactoryInterface
{
    private $writer;
    private $cmd;

    public function __construct(Writer $writer, string $cmd)
    {
        $this->writer = $writer;
        $this->cmd = $cmd;
    }
    public function createCommand(): Command
    {
        return new UnknownCommand($this->writer, $this->cmd);
    }
}