<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 13:06
 */

namespace BinaryStudioAcademy\Game\CommandFactory;


use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Command\ExitCommand;
use BinaryStudioAcademy\Game\Contracts\CommandFactory\CommandFactoryInterface;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class ExitCommandFactory implements CommandFactoryInterface
{
    private $writer;

    public function __construct(Writer $writer)
    {
        $this->writer = $writer;
    }

    public function createCommand(): Command
    {
        return new ExitCommand($this->writer);
    }
}