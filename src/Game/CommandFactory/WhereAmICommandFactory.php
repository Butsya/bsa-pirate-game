<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 13:11
 */

namespace BinaryStudioAcademy\Game\CommandFactory;

use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Command\WhereAmICommand;
use BinaryStudioAcademy\Game\Contracts\CommandFactory\CommandFactoryInterface;
use BinaryStudioAcademy\Game\Contracts\Helpers\PlayerPositionInterface;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class WhereAmICommandFactory implements CommandFactoryInterface
{
    private $writer;
    private $playerPosition;

    public function __construct(Writer $writer, PlayerPositionInterface $playerPosition)
    {
        $this->writer = $writer;
        $this->playerPosition = $playerPosition;
    }

    public function createCommand(): Command
    {
        return new WhereAmICommand($this->writer, $this->playerPosition);
    }
}