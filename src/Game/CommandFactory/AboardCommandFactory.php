<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 14:18
 */

namespace BinaryStudioAcademy\Game\CommandFactory;


use BinaryStudioAcademy\Game\Builder\Parts\Ship;
use BinaryStudioAcademy\Game\Command\AboardCommand;
use BinaryStudioAcademy\Game\Command\Errors\AboardNotSunkedShipCommand;
use BinaryStudioAcademy\Game\Command\Errors\AboardPirateHarborCommand;
use BinaryStudioAcademy\Game\Command\Errors\HoldIsFullCommand;
use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\CommandFactory\CommandFactoryInterface;
use BinaryStudioAcademy\Game\Contracts\Helpers\PlayerPositionInterface;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Mapper\ShipMapperInterface;

class AboardCommandFactory implements CommandFactoryInterface
{
    private $shipMapper;
    private $writer;
    private $playerPosition;

    public function __construct(
        Writer $writer,
        ShipMapperInterface $shipMapper,
        PlayerPositionInterface $playerPosition
    ) {
        $this->writer = $writer;
        $this->shipMapper = $shipMapper;
        $this->playerPosition = $playerPosition;
    }

    public function createCommand(): Command
    {
        $command = null;
        if ($this->playerPosition->getPosition() === 1) {
            $command = new AboardPirateHarborCommand($this->writer);
        } elseif($this->shipMapper->getShip('enemy')->getStat('health') > 0){
            $command = new AboardNotSunkedShipCommand($this->writer);
        } elseif(!strpos($this->shipMapper->getShip('player')->getStat('hold'), Ship::EMPTY)) {
            $command = new HoldIsFullCommand($this->writer);
        } else {
            $command = new AboardCommand($this->shipMapper, $this->writer);
        }

        return $command;
    }
}
