<?php

namespace BinaryStudioAcademy\Game\CommandFactory;

use BinaryStudioAcademy\Game\Command\BuyRumCommand;
use BinaryStudioAcademy\Game\Command\BuySkillCommand;
use BinaryStudioAcademy\Game\Command\Errors\BuyInPirateHarborCommand;
use BinaryStudioAcademy\Game\Command\Errors\NotEnoughGoldCommand;
use BinaryStudioAcademy\Game\Command\Errors\SkillMaxValueCommand;
use BinaryStudioAcademy\Game\Command\Errors\UnknownItemCommand;
use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Builder\Parts\Ship;
use BinaryStudioAcademy\Game\Contracts\CommandFactory\CommandFactoryInterface;
use BinaryStudioAcademy\Game\Contracts\Helpers\PlayerPositionInterface;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Mapper\ShipMapperInterface;

class BuyCommandFactory implements CommandFactoryInterface
{
    private $writer;
    private $shipMapper;
    private $playerPosition;
    private $cmd;

    public function __construct(
        Writer $writer,
        ShipMapperInterface $shipMapper,
        PlayerPositionInterface $playerPosition,
        string $cmd
    ) {
        $this->writer = $writer;
        $this->shipMapper = $shipMapper;
        $this->playerPosition = $playerPosition;
        $this->cmd = $cmd;
    }

    public function createCommand(): Command
    {
        $command = null;
        $item = $this->getCommandArgument();
        $playerShip = $this->shipMapper->getShip('player');

        if ($item === 'rum' || in_array($item, ['strength','armour','luck'])) {
            if ($this->isPlayerInThePirateHarbor()) {
                if ($this->isPlayerHasGold($playerShip)) {
                    if ($item === 'rum') {
                        $command = new BuyRumCommand($this->writer, $this->shipMapper);
                    } elseif ($playerShip->getStat($item) < 10) {
                        $command = new BuySkillCommand($this->writer, $this->shipMapper, $item);
                    } else {
                        $command = new SkillMaxValueCommand($this->writer, $item);
                    }
                } else {
                    $command = new NotEnoughGoldCommand($this->writer);
                }
            } else {
                $command = new BuyInPirateHarborCommand($this->writer);
            }
        } else {
            $command = new UnknownItemCommand($this->writer);
        }
        
        return $command;
    }

    private function isPlayerInThePirateHarbor()
    {
        return $this->playerPosition->getPosition() === 1;
    }

    private function isPlayerHasGold(Ship $playerShip)
    {
        return strpos($playerShip->getStat('hold'), Ship::GOLD) !== false;
    }

    private function getCommandArgument()
    {
        $commandArray = explode(' ', $this->cmd);
        return array_pop($commandArray);
    }
}
