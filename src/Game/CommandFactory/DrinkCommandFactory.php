<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 13:05
 */

namespace BinaryStudioAcademy\Game\CommandFactory;

use BinaryStudioAcademy\Game\Builder\Parts\Ship;
use BinaryStudioAcademy\Game\Command\DrinkCommand;
use BinaryStudioAcademy\Game\Command\Errors\RumAbsentInHoldCommand;
use BinaryStudioAcademy\Game\Contracts\CommandFactory\CommandFactoryInterface;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\Mapper\ShipMapperInterface;

class DrinkCommandFactory implements CommandFactoryInterface
{
    private $writer;

    private $shipMapper;

    public function __construct(Writer $writer, ShipMapperInterface $shipMapper)
    {
        $this->writer = $writer;
        $this->shipMapper = $shipMapper;
    }

    public function createCommand(): Command
    {
        $command = null;
        $playerShip = $this->shipMapper->getShip('player');
        if (strpos($playerShip->getStat('hold'), Ship::RUM)) {
            $command = new DrinkCommand($this->writer, $this->shipMapper);
        } else {
            $command = new RumAbsentInHoldCommand($this->writer);
        }
        return $command;
    }
}
