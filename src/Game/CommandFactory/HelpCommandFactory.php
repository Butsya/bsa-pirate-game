<?php

namespace BinaryStudioAcademy\Game\CommandFactory;

use BinaryStudioAcademy\Game\Command\HelpCommand;
use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\CommandFactory\CommandFactoryInterface;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;

class HelpCommandFactory implements CommandFactoryInterface
{
    private $writer;

    public function __construct(Writer $writer)
    {
        $this->writer = $writer;
    }
    public function createCommand(): Command
    {
        return new HelpCommand($this->writer);
    }
}