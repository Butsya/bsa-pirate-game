<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 22:49
 */

namespace BinaryStudioAcademy\Game\CommandFactory;

use BinaryStudioAcademy\Game\Command\StatsCommand;
use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\CommandFactory\CommandFactoryInterface;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Mapper\ShipMapperInterface;

class StatsCommandFactory implements CommandFactoryInterface
{
    private $writer;

    private $shipMapper;

    public function __construct(Writer $writer, ShipMapperInterface $shipMapper)
    {
        $this->writer = $writer;
        $this->shipMapper = $shipMapper;
    }

    public function createCommand(): Command
    {
        return new StatsCommand($this->writer, $this->shipMapper->getShip('player'));
    }
}
