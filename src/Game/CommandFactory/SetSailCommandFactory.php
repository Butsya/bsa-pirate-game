<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 13:08
 */

namespace BinaryStudioAcademy\Game\CommandFactory;


use BinaryStudioAcademy\Game\Command\Errors\DirectionNotExistCommand;
use BinaryStudioAcademy\Game\Command\Errors\IncorrectDirectionCommand;
use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Command\SetSailCommand;
use BinaryStudioAcademy\Game\Contracts\CommandFactory\CommandFactoryInterface;
use BinaryStudioAcademy\Game\Contracts\Helpers\Map;
use BinaryStudioAcademy\Game\Contracts\Helpers\PlayerPositionInterface;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Mapper\ShipMapperInterface;

class SetSailCommandFactory implements CommandFactoryInterface
{
    private $writer;
    private $shipMapper;
    private $playerPosition;
    private $cmd;

    public function __construct(
        Writer $writer,
        ShipMapperInterface $shipMapper,
        PlayerPositionInterface $playerPosition,
        string $cmd
    ) {
        $this->writer = $writer;
        $this->shipMapper = $shipMapper;
        $this->playerPosition = $playerPosition;
        $this->cmd = $cmd;
    }

    public function createCommand(): Command
    {
        $command = null;
        $direction = $this->getCommandArgument();

        if (array_key_exists($direction, Map::MAP[$this->playerPosition->getPosition()])) {
            $command = new SetSailCommand(
                $this->writer, $this->shipMapper, $this->playerPosition, $direction
            );
        } elseif (!in_array($direction, ['east','west','north','south'])) {
            $command = new DirectionNotExistCommand($this->writer, $direction);
        } else {
            $command = new IncorrectDirectionCommand($this->writer);
        }

        return $command;
    }

    private function getCommandArgument()
    {
        $commandArray = explode(' ', $this->cmd);
        return array_pop($commandArray);
    }
}
