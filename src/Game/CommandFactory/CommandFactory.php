<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 15:36
 */

namespace BinaryStudioAcademy\Game\CommandFactory;

use BinaryStudioAcademy\Game\Builder\Parts\Ship;
use BinaryStudioAcademy\Game\CommandFactory\Errors\UnknownCommandFactory;
use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Contracts\CommandFactory\CommandFactoryInterface;
use BinaryStudioAcademy\Game\Contracts\Helpers\Math;
use BinaryStudioAcademy\Game\Contracts\Helpers\PlayerPositionInterface;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Mapper\ShipMapperInterface;

class CommandFactory implements CommandFactoryInterface
{
    private $cmd;
    private $writer;
    private $playerShip;
    private $playerPosition;
    private $enemyShip;
    private $random;
    private $math;
    private $shipMapper;

    public function __construct(
        Writer $writer,
        PlayerPositionInterface $playerPosition,
        Math $math,
        Random $random,
        ShipMapperInterface $shipMapper,
        string $cmd
    ) {
        $this->writer = $writer;
        $this->cmd = $cmd;
        $this->playerPosition = $playerPosition;
        $this->random = $random;
        $this->math = $math;
        $this->shipMapper = $shipMapper;
    }

    public function createCommand(): Command
    {
        $commandFactory = new UnknownCommandFactory($this->writer, $this->cmd);
        switch ($this->cmd) {
            case 'help':
                $commandFactory = new HelpCommandFactory($this->writer);
                break;
            case 'stats':
                $commandFactory = new StatsCommandFactory($this->writer, $this->shipMapper);
                break;
            case 'drink':
                $commandFactory = new DrinkCommandFactory($this->writer, $this->shipMapper);
                break;
            case strpos($this->cmd,'buy'):
                $commandFactory = new BuyCommandFactory(
                    $this->writer, $this->shipMapper, $this->playerPosition, $this->cmd
                );
                break;
            case strpos($this->cmd,'set-sail'):
                $commandFactory = new SetSailCommandFactory(
                        $this->writer, $this->shipMapper, $this->playerPosition, $this->cmd
                );
                break;
            case 'fire':
                $commandFactory = new FireCommandFactory(
                    $this->writer, $this->shipMapper, $this->playerPosition, $this->math, $this->random
                );
                break;
            case 'aboard':
                $commandFactory = new AboardCommandFactory(
                    $this->writer, $this->shipMapper, $this->playerPosition
                );
                break;
            case 'whereami':
                $commandFactory = new WhereAmICommandFactory($this->writer, $this->playerPosition);
                break;
            case 'exit':
                $commandFactory = new ExitCommandFactory($this->writer);
                break;
        }

        return $commandFactory->createCommand();
    }
}
