<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 13:12
 */

namespace BinaryStudioAcademy\Game\CommandFactory;


use BinaryStudioAcademy\Game\Command\Errors\FireInPirateHarborIsNotAllowedCommand;
use BinaryStudioAcademy\Game\Command\Errors\ShipIsAlreadySunkedCommand;
use BinaryStudioAcademy\Game\Contracts\Command\Command;
use BinaryStudioAcademy\Game\Command\FireCommand;
use BinaryStudioAcademy\Game\Contracts\CommandFactory\CommandFactoryInterface;
use BinaryStudioAcademy\Game\Contracts\Helpers\Math;
use BinaryStudioAcademy\Game\Contracts\Helpers\PlayerPositionInterface;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Mapper\ShipMapperInterface;

class FireCommandFactory implements CommandFactoryInterface
{
    private $shipMapper;
    private $writer;
    private $random;
    private $math;
    private $playerPosition;

    public function __construct(
        Writer $writer,
        ShipMapperInterface $shipMapper,
        PlayerPositionInterface $playerPosition,
        Math $math,
        Random $random
    ) {
        $this->writer = $writer;
        $this->shipMapper = $shipMapper;
        $this->playerPosition = $playerPosition;
        $this->math = $math;
        $this->random = $random;
    }

    public function createCommand(): Command
    {
        $command = null;

        if ($this->playerPosition->getPosition() === 1) {
            $command = new FireInPirateHarborIsNotAllowedCommand($this->writer);
        } elseif($this->shipMapper->getShip('enemy')->getStat('health') <= 0) {
            $command = new ShipIsAlreadySunkedCommand($this->writer);
        } else {
            $command = new FireCommand(
                $this->shipMapper, $this->writer, $this->math, $this->random, $this->playerPosition
            );
        }

        return $command;
    }
}
