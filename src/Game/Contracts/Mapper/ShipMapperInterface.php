<?php
/**
 * Created by PhpStorm.
 * User: bucya
 * Date: 7/8/19
 * Time: 3:39 PM
 */

namespace BinaryStudioAcademy\Game\Contracts\Mapper;


use BinaryStudioAcademy\Game\Builder\Parts\Ship;

interface ShipMapperInterface
{
    public function setShip(string $type, Ship $ship);

    public function getShip(string $type): Ship;
}
