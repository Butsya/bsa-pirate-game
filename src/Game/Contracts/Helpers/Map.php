<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 07.07.19
 * Time: 0:19
 */

namespace BinaryStudioAcademy\Game\Contracts\Helpers;

use BinaryStudioAcademy\Game\Builder\HmsRoyalSovereignShipBuilder;
use BinaryStudioAcademy\Game\Builder\RoyalBattleShipBuilder;
use BinaryStudioAcademy\Game\Builder\RoyalPatrolSchoonerShipBuilder;

interface Map
{
    const HARBORS = [
        1 => [
            'harbor' => 'Pirates Harbor',
            'ship' => 'player'
        ],
        2 => [
            'harbor' => 'Southhampton',
            'ship' => 'schooner'
        ],
        3 => [
            'harbor' => 'Fishguard',
            'ship' => 'schooner'
        ],
        4 => [
            'harbor' => 'Salt End',
            'ship' => 'schooner'
        ],
        5 => [
            'harbor' => 'Isle of Grain',
            'ship' => 'schooner'
        ],
        6 => [
            'harbor' => 'Grays',
            'ship' => 'battle'
        ],
        7 => [
            'harbor' => 'Felixstowe',
            'ship' => 'battle'
        ],
        8 => [
            'harbor' => 'London Docks',
            'ship' => 'royal'
        ]
    ];

    const MAP = [
        1 => [
            'south' => 2,
            'north' => 4,
            'west' => 3,
        ],
        2 => [
            'east' => 7,
            'north' => 1,
            'west' => 3,
        ],
        3 => [
            'east' => 1,
            'north' => 4,
            'south' => 2,
        ],
        4 => [
            'east' => 5,
            'west' => 3,
            'south' => 1,
        ],
        5 => [
            'east' => 6,
            'west' => 4,
            'south' => 7,
        ],
        6 => [
            'west' => 5,
            'south' => 8,
        ],
        7 => [
            'east' => 8,
            'north' => 5,
            'west' => 2,
        ],
        8 => [
            'west' => 7,
            'north' => 6,
        ]
    ];

    const SHIPS = [
        'schooner' => [
            'name' => 'Royal Patrool Schooner',
            'stats' => [
                'strength' => 4,
                'armour' => 4,
                'luck' => 4,
                'health' => 50,
            ],
            'shipBuilder' => RoyalPatrolSchoonerShipBuilder::class,
        ],
        'battle' => [
            'name' => 'Royal Battle Ship',
            'stats' => [
                'strength' => 8,
                'armour' => 8,
                'luck' => 7,
                'health' => 80,
            ],
            'shipBuilder' => RoyalBattleShipBuilder::class,
        ],
        'royal' => [
            'name' => 'HMS Royal Sovereign',
            'stats' => [
                'strength' => 10,
                'armour' => 10,
                'luck' => 10,
                'health' => 100,
            ],
            'shipBuilder' => HmsRoyalSovereignShipBuilder::class,
        ],
        'player' => [],
    ];
}
