<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 07.07.19
 * Time: 0:38
 */

namespace BinaryStudioAcademy\Game\Contracts\Helpers;


interface PlayerPositionInterface
{
    public function getPosition();

    public function setPosition(int $position);
}