<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 15:33
 */

namespace BinaryStudioAcademy\Game\Contracts\CommandFactory;

use BinaryStudioAcademy\Game\Contracts\Command\Command;

interface CommandFactoryInterface
{
    public function createCommand(): Command;
}