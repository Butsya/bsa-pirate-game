<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 06.07.19
 * Time: 22:50
 */

namespace BinaryStudioAcademy\Game\Contracts\Builder;

use BinaryStudioAcademy\Game\Builder\Parts\Ship;

interface BuilderInterface
{
    public function createShip();

    public function addStrength();

    public function addArmour();

    public function addLuck();

    public function addHealth();

    public function addHold();

    public function getShip(): Ship;
}