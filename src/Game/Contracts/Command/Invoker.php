<?php
/**
 * Created by PhpStorm.
 * User: skozar
 * Date: 07.07.19
 * Time: 2:36
 */

namespace BinaryStudioAcademy\Game\Contracts\Command;


interface Invoker
{
    public function setCommand(Command $cmd);

    public function run();
}